function cambio() {
    document.getElementById("titulo").style.fontStyle = "italic";
}

function normal() {
    document.getElementById("titulo").style.fontStyle = "normal";
}

function campos() {
    let name, st, num, col, mail, card, date, code;
    name=document.getElementById("name").value;
    st=document.getElementById("st").value;
    num=document.getElementById("num").value;
    col=document.getElementById("col").value;
    mail=document.getElementById("mail").value;
    card=document.getElementById("card").value;
    date=document.getElementById("date").value;
    code=document.getElementById("code").value;
    if (name==="" || st===""|| num===""|| col===""|| mail===""|| card===""|| date===""|| code===""){
        alert("Todos los campos son obligatorios, para mas detalles sobre algun campo da clic en el boton de informacion")
        return false;
    } else {
        alert("Registro Completado con Exito!")
        return true;
    }
}

function timeout() {
    setTimeout(function(){ alert("Nombre: Nombre completo del comprador\n\nCalle: Calle del domicilio\n\nNumero: Numero exterior del domicilio\n\nColonia: Colonia del domicilio\n\nCorreo Electronico: Direccion de correo electronico para recibir la factura electronica\n\nNumero de Tarjeta: 16 digitos de la tarjeta de credito o debito\n\nFecha de vencimiento: coloca el dia en 1 y completa con el mes y año que aparecen en la tarjeta\n\nCodigo de Seguridad: Los tres digitos al reverso de la tarjeta"); }, 500);
}

function allowDrop(ev) {
    ev.preventDefault();
}

function drag(ev) {
    ev.dataTransfer.setData("text", ev.target.id);
}

function drop(ev) {
    ev.preventDefault();
    var data = ev.dataTransfer.getData("text");
    ev.target.appendChild(document.getElementById(data));
}
<!Doctype html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Laboratorio 9</title>
    </head>
    <body>
        <h1>Laboratorio 9</h1>
        <h2>Problemas</h2>
        <h3>Problema 1</h3>
        <?php
            $arreglo = array(2, 4, 6, 8, 10, 12, 14, 16, 18, 20);
            $aux = 0;
            for ($i=0; $i < count($arreglo);$i++)
            {
                $aux+=$arreglo[$i];
            }
            $aux/=10;
            echo "El Promedio del arreglo (2, 4, 6, 8, 10, 12, 14, 16, 18, 20) es $aux";
        ?>
        <h3>Problema 2</h3>
        <?php
            $arreglo = array(1, 4, 5, 8, 9, 12, 13, 16, 17, 20, 21);
            $aux=count($arreglo)/2;
            echo "La mediana del arreglo (1, 4, 5, 8, 9, 12, 13, 16, 17, 20, 21) es $arreglo[$aux]";
        ?>
        <h3>Problema 3</h3>
        <?php
            $arreglo = array(3, 5, 6, 7, 9, 11, 13, 18, 19, 20, 21);
            $promedio = 0;
            $j=count($arreglo);
            for ($i=0; $i < $j;$i++)
            {
                $promedio+=$arreglo[$i];
            }
            $promedio/=$j;
            $aux=count($arreglo)/2;
            $mediana=$arreglo[$aux];
            $j--;
            echo " <Table> <tr>
                        <td>Menor a Mayor</td>
                        <td>Mayor a Menor</td>
                    </tr>";
            for ($i=0; $i < count($arreglo);$i++)
            {
                echo "
                <tr>
                        <td>$arreglo[$i]</td>
                        <td>$arreglo[$j]</td>
                    </tr>
                    ";
                $j--;
            }
       
        echo "
                <tr>
                    <td>Promedio</td>
                    <td>Mediana</td>
                </tr>
                <tr>
                    <td>$promedio</td>
                    <td>$mediana</td>
                </tr>
            </table>";
        ?>
        <h3>Problema 5</h3>
        <?php
            $n=10;
            echo " <Table> <tr>
                        <th>n</th>
                        <th>n^2</th>
                        <th>n^3</th>
                    </tr>";
            for ($i=1; $i<=$n;$i++)
                {
                    echo "
                    <tr>
                            <td>$i</td>
                            <td>". $i*$i ."</td>
                            <td>". ($i*$i*$i) . "</td>
                        </tr>
                        ";
                }
        ?>
    </body>
</html>
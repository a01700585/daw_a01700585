function validar(){
    let psw, psw1;
    psw=document.getElementById("psw").value;
    psw1=document.getElementById("psw1").value;
    if (psw==="" || psw1===""){
        alert("Escribe la contraseña")
        return false;
    } else if (psw!=psw1){
        alert("La contraseña no coincide")
        return false;
    } else if(psw==psw1){
        alert("Validacion correcta")
        return true;
    }
}
function producto1(){
    let color, total, cant, precio=5000, iva=800;
    color=document.getElementById("color1").value;
    cant=document.getElementById("cant1").value;
    if(color===""||cant===""){
        alert("Llena todos los campos")
        return false;
    } else {
        precio=precio*cant;
        iva=iva*cant;
        total=precio+iva;
        alert("Precio + iva: "+precio+" + "+iva+"\n"+"total: "+total);
        return true;
    }
}

function producto2(){
    let color, total, cant, precio=700, iva=112;
    color=document.getElementById("color2").value;
    cant=document.getElementById("cant2").value;
    if(cant===""){
        alert("Llena todos los campos")
        return false;
    } else {
        precio=precio*cant;
        iva=iva*cant;
        total=precio+iva;
        alert("Precio + iva: "+precio+" + "+iva+"\n"+"total: "+total);
        return true;
    }
}

function producto3(){
    let color, total, cant, precio=400, iva=64;
    color=document.getElementById("color3").value;
    cant=document.getElementById("cant3").value;
    if(color===""||cant===""){
        alert("Llena todos los campos")
        return false;
    } else {
        precio=precio*cant;
        iva=iva*cant;
        total=precio+iva;
        alert("Precio + iva: "+precio+" + "+iva+"\n"+"total: "+total);
        return true;
    }
}

function registro(){
    let name, cumple, correo, deporte, posicion, contra1, contra2;
    name=document.getElementById("name").value;
    cumple=document.getElementById("cumple").value;
    correo=document.getElementById("correo").value;
    deporte=document.getElementById("deporte").value;
    posicion=document.getElementById("posicion").value;
    contra1=document.getElementById("contra1").value;
    contra2=document.getElementById("contra2").value;
    if (name==="" || cumple===""|| correo===""|| deporte===""|| posicion===""|| contra1===""|| contra2===""){
        alert("Todos los campos son obligatorios")
        return false;
    } else if (contra1!=contra2){
        alert("La contraseña no coincide")
        return false;
    } else if(contra1==contra2){
        alert("Registro Completado con Exito!")
        return true;
    }
}
<?php

		function conecta(){
			$mysql = mysqli_connect("localhost","root","","examen");
			$mysql->set_charset("utf8");
			return $mysql;
		}

		function desconecta($mysql){
			mysqli_close($mysql);
		}

		function selectZombies(){
			$db = conecta();
			$query = 'SELECT * FROM zombies';
			$result = mysqli_query($db,$query);
			desconecta($db);
			return $result;
		}

		function getZombies($db,$zombieId){
			$query='SELECT * FROM zombies WHERE zombieId="'.$zombieId.'"';
		    $result = mysqli_query($db,$query);
			$fila = mysqli_fetch_array($result,MYSQLI_BOTH);
		    return $fila;
		}

		function showZombies(){
			$result = selectZombies();
			echo "<table class=bordered>";
			echo "<tr>";
			echo "<th>Nombre</th>";
			echo "<th>Estado</th>";
			echo "<th>Fecha de Registro</th>";
			echo "<th>Fecha de Modificacion</th>";
			echo "<th>Editar</th>";
			echo "<th>Eliminar</th>";
			echo "</tr>";
			while($fila = mysqli_fetch_array($result,MYSQLI_BOTH)){
				echo "<tr>";
				echo "<td>" . $fila["nombre"] . "</td>";
				echo "<td>" . $fila["estado"] . "</td>";
				echo "<td>" . $fila["registro"] . "</td>";
				echo "<td>" . $fila["modificacion"] . "</td>";
				echo "<td><a href=\"EditarZombieExamen.php?zombieId=$fila[zombieId]\" class=\"btn-floating btn-large waves-effect waves-light green darken-1\"><i class=\"material-icons\">edit</i></a></td>";
				echo "<td><a href=\"EliminarZombieExamen.php?zombieId=$fila[zombieId]\" class=\"btn-floating btn-large waves-effect waves-light red darken-1\"><i class=\"material-icons\">delete</i></a></td>";
				echo "</tr>";
			}
			echo "</table>";
		} 

		function guardarZombie($nombre,$estado){
		$db = conecta();

		$query = 'INSERT INTO zombies (nombre,estado) VALUES (?,?)';

		if (!($statement = $db->prepare($query))) {
	    die("Preparation failed: (" . $db->errno . ") " . $db->error);
	    }
	    // Binding statement http_parse_params(

	    	//ESTO ES LO UNICO QUE SE TOCA
	    if (!$statement->bind_param("ss", $nombre, $estado)){
	        die("Parameter vinculation failed: (" . $statement->errno . ") " . $statement->error); 
	    }

	    // Executing the statement
	    if (!$statement->execute()) {
	        die("Execution failed: (" . $statement->errno . ") " . $statement->error);
	     }
		}


		function eliminaZombie($zombieId){
			$db = conecta();

			$query = "DELETE FROM zombies WHERE zombieId = $zombieId";

			mysqli_query($db,$query);

			desconecta($db);
		}

		function editarZombie($zombieId, $nombre, $estado){
		    $db = conectar();
		    // insert command specification 
		    $query="UPDATE zombies SET zombieId=?,nombre=?,estado=? WHERE zombieId LIKE ? ";
		    // Preparing the statement 
		    if (!($statement = $db->prepare($query))) {
		        die("Preparation failed: (" . $db->errno . ") " . $db->error);
		    }
		    // Binding statement params 
		    if (!$statement->bind_param("ss", $nombre, $estado)) {
		        die("Parameter vinculation failed: (" . $statement->errno . ") " . $statement->error); 
		    }
		    // update execution
		    if ($statement->execute()) {
		        echo 'There were ' . mysqli_affected_rows($mysql) . ' affected rows';
		    } else {
		        die("Update failed: (" . $statement->errno . ") " . $statement->error);
		    }
		    desconectar($db);
		}


		function totalZombies($registro){
			$db = conecta();
			$query = "SELECT COUNT(*) FROM zombies";
			$result = mysqli_query($db,$query);
			desconecta($db);
			return $result;
		}

		function estadosZombie($registro){
		$db = conecta();
		$query = "SELECT DISTINCT estado FROM zombies";
		$result = mysqli_query($db,$query);
		desconecta($db);
		return $result;
		}

		function noMuertos($registro){
			$db = conecta();
			$query = "SELECT * FROM zombies WHERE estado LIKE '%$coma%' OR estado LIKE '%$infec%' OR estado LIKE '%$tran%'";
			$result = mysqli_query($db,$query);
			desconecta($db);
		return $result;
		}

		function fechaRegistroZombies($registro){
			$db = conecta();
			$query = "SELECT * FROM zombies ORDER BY registro";
			$result = mysqli_query($db,$query);
			desconecta($db);
			return $result;
		}

		function estadoNumero($estado,$numero){
			$db = conecta();
			$query = "SELECT TOP $numero estado FROM zombies WHERE nombre LIKE '%$estado%'";
			$result = mysqli_query($db,$query);
			desconecta($db);
			return $result;
		}

		function noMuertos($registro){
			$db = conecta();
			$query = "SELECT * FROM zombies WHERE estado LIKE '%$coma%' OR estado LIKE '%$infec%' OR estado LIKE '%$tran%'";
			$result = mysqli_query($db,$query);
			desconecta($db);
		return $result;
		}
?>
-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 27-10-2017 a las 23:01:51
-- Versión del servidor: 10.1.26-MariaDB
-- Versión de PHP: 7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `examen`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `zombies`
--

CREATE TABLE `zombies` (
  `nombre` varchar(100) NOT NULL,
  `estado` varchar(100) NOT NULL,
  `registro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modificacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `zombieId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `zombies`
--

INSERT INTO `zombies` (`nombre`, `estado`, `registro`, `modificacion`, `zombieId`) VALUES
('JIAV', 'completamente muerto', '2017-10-27 19:26:39', '2017-10-27 19:26:39', 2),
('Erwin', 'coma', '2017-10-27 19:43:40', '2017-10-27 19:43:40', 5),
('Octavio', 'transformacion', '2017-10-27 19:44:09', '2017-10-27 19:44:09', 6),
('Alan', 'infección', '2017-10-27 19:51:35', '2017-10-27 19:51:35', 7),
('Betanzos', 'Infección', '2017-10-27 19:57:21', '2017-10-27 19:57:21', 8),
('Betanzos', 'Infección', '2017-10-27 19:57:21', '2017-10-27 19:57:21', 9),
('Romel', 'Completamente Muerto', '2017-10-27 19:57:41', '2017-10-27 19:57:41', 10),
('Karla', 'Coma', '2017-10-27 19:59:03', '2017-10-27 19:59:03', 11),
('Raygadas', 'Transformacion', '2017-10-27 19:59:23', '2017-10-27 19:59:23', 12),
('Hockey', 'Coma', '2017-10-27 19:59:36', '2017-10-27 19:59:36', 13),
('Hockey', 'Coma', '2017-10-27 20:21:46', '2017-10-27 20:21:46', 15);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `zombies`
--
ALTER TABLE `zombies`
  ADD PRIMARY KEY (`zombieId`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `zombies`
--
ALTER TABLE `zombies`
  MODIFY `zombieId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

<?php
	session_start();
	require_once("ModeloExamen.php");
	
	if(isset($_SESSION["user"])){
		include("HeaderExamen.html");
		include("IndexExamen.html");
		echo showZombies();
		include("FooterExamen.html");
	}
	else if(isset($_POST["usuario"]) && isset($_POST["contraseña"])){
		$_SESSION["user"] = $_POST["usuario"];
		include("HeaderExamen.html");
		include("IndexExamen.html");
		echo showZombies();
		include("FooterExamen.html");
	}
	else{
		header("location:LoginExamen.php");
	}

?>
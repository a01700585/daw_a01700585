BULK INSERT a1700585.a1700585.[Entregan]
   FROM 'e:\wwwroot\a1700585\entregan.csv'
   WITH
      (
         CODEPAGE = 'ACP',
         FIELDTERMINATOR = ',',
         ROWTERMINATOR = '\n'
      )
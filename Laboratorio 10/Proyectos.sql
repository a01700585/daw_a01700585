BULK INSERT a1700585.a1700585.[Proyectos]
   FROM 'e:\wwwroot\a1700585\proyectos.csv'
   WITH
      (
         CODEPAGE = 'ACP',
         FIELDTERMINATOR = ',',
         ROWTERMINATOR = '\n'
      )
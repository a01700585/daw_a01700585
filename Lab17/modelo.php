<?php

function conectar() {
    $mysql = mysqli_connect("localhost","root","","laboratorio17");
    $mysql->set_charset("utf8");
    return $mysql;
}

function desconectar($mysql) {
    mysqli_close($mysql);
}

function getTabla($tabla) {
    $db = conectar();
    
    //Specification of the SQL query
    $query='SELECT * FROM '.$tabla;
     // Query execution; returns identifier of the result group
    $registros = $db->query($query);
     // cycle to explode every line of the results
    while ($fila = mysqli_fetch_array($registros, MYSQLI_BOTH)) {
                                                // Options: MYSQLI_NUM to use only numeric indexes
                                                // MYSQLI_ASSOC to use only name (string) indexes
                                                // MYSQLI_BOTH, to use both
        for($i=0; $i<count($fila); $i++) {
            // use of numeric index
            echo $fila[$i].' '; 
        }
        echo '<br>';
    }
    echo '<br>';
    // it releases the associated results
    mysqli_free_result($registros);
    
    desconectar($db);
}

function getEvento($db, $eventosId){
    //Specification of the SQL query
    $query='SELECT * FROM eventos WHERE id="'.$eventosId.'"';
     // Query execution; returns identifier of the result group
    $registros = $db->query($query);   
    $fila = mysqli_fetch_array($registros, MYSQLI_BOTH);
    return $fila;
}

function getEventosCards() {
    $db = conectar();
    
    //Specification of the SQL query
    $query='SELECT * FROM eventos ORDER BY fecha desc';
     // Query execution; returns identifier of the result group
    $registros = $db->query($query);
    
    
    $cards = "";
     // cycle to explode every line of the results
    while ($fila = mysqli_fetch_array($registros, MYSQLI_BOTH)) {
                                                // Options: MYSQLI_NUM to use only numeric indexes
                                                // MYSQLI_ASSOC to use only name (string) indexes
                                                // MYSQLI_BOTH, to use both
        
    	$cards .= '
    	<div class="row">
        <div class="col s12 m6">
          <div class="card blue-grey darken-1">
            <div class="card-content white-text">
              <span class="card-title">'.$fila["titulo"].'</span>
              <p>'.$fila["descripcion"].'</p>
              <br><br>
              <p>'.$fila["direccion"].'</p>
              <br><br>
              <p>'.$fila["fecha"].'</p>
            </div>
            <div class="card-action">
             <a href="editar.php?id='.$fila["id"].'">Editar</a>
            </div>
          </div>
        </div>
      </div>';
      
    }
    // it releases the associated results
    mysqli_free_result($registros);
    
    desconectar($db);
    
    return $cards;
}

function getEventosTable() {
    $db = conectar();
    
    //Specification of the SQL query
    $query='SELECT * FROM eventos';
     // Query execution; returns identifier of the result group
    $registros = $db->query($query);
    
    
    $table = '<table class="striped">
        <thead>
          <tr>
              <th>Titulo</th>
              <th>Descripcion</th>
              <th>Direccion</th>
              <th>Fecha</th>
          </tr>
        </thead>
        <tbody>';
     // cycle to explode every line of the results
    while ($fila = mysqli_fetch_array($registros, MYSQLI_BOTH)) {
                                                // Options: MYSQLI_NUM to use only numeric indexes
                                                // MYSQLI_ASSOC to use only name (string) indexes
                                                // MYSQLI_BOTH, to use both
        
    	$table .= '
    	<tr>
            <td>'.$fila["titulo"].'</td>
            <td>'.$fila["descripcion"].'</td>
            <td>'.$fila["direccion"].'</td>
            <td>'.$fila["fecha"].'</td>
        </tr>';
      
    }
    // it releases the associated results
    mysqli_free_result($registros);
    
    desconectar($db);
    
    $table .= "</tbody></table>";
    
    return $table;
}

function guardarEvento($titulo, $descripcion, $direccion){
    $db = conectar();
    
    // insert command specification 
    $query='INSERT INTO eventos (`titulo`, `descripcion` ,`direccion`) VALUES (?,?,?) ';
    // Preparing the statement 
    if (!($statement = $db->prepare($query))) {
        die("Preparation failed: (" . $db->errno . ") " . $db->error);
    }
    // Binding statement params 
    if (!$statement->bind_param("sss", $titulo, $descripcion, $direccion)) {
        die("Parameter vinculation failed: (" . $statement->errno . ") " . $statement->error); 
    }
    // Executing the statement
    if (!$statement->execute()) {
        die("Execution failed: (" . $statement->errno . ") " . $statement->error);
     } 

    
    desconectar($db);
}

function editarEvento($id, $titulo, $descripcion, $direccion){
    $db = conectar();
    
    // insert command specification 
    $query='UPDATE eventos SET titulo=?, descripcion=?, direccion=? WHERE  ';
    // Preparing the statement 
    if (!($statement = $db->prepare($query))) {
        die("Preparation failed: (" . $db->errno . ") " . $db->error);
    }
    // Binding statement params 
    if (!$statement->bind_param("ssss", $titulo, $descripcion, $direccion, $id)) {
        die("Parameter vinculation failed: (" . $statement->errno . ") " . $statement->error); 
    }
    // update execution
    if ($statement->execute()) {
        echo 'There were ' . mysqli_affected_rows($mysql) . ' affected rows';
    } else {
        die("Update failed: (" . $statement->errno . ") " . $statement->error);
    }
 

    
    desconectar($db);
}

?>
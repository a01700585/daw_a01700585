<?php
    session_start();
    require_once("modelo.php");
    if(isset($_POST["id"])) {
        editarEvento($_POST["id"], $_POST["titulo"], $_POST["descripcion"], $_POST["direccion"], $_POST["fecha"]);
        $_SESSION["mensaje"] = $_POST["titulo"].' se actualizó correctamente';
    } else {
        guardarEvento($_POST["titulo"], $_POST["descripcion"], $_POST["direccion"], , $_POST["fecha"]);
        $_SESSION["mensaje"] = $_POST["titulo"].' se registró correctamente';
    }
    header("location:index.php");
?>
<?php

function connectDB(){
    
    $servername= "localhost";
    $username = "root";
    $password = "";
    $dbname = "Playeras";

    $mysql = mysqli_connect($servername, $username, $password, $dbname);

    // Check connection 
    if (!$mysql) {
        die("Connection failed: " . mysqli_connect_error());
    }
    
    return $mysql;
}

function closeDB($mysql){
    
    mysqli_close($mysql);
    
}

function getPlayeras($playeras){
    
    $db = connectDB();
    $sql = 'SELECT * FROM '.$playeras;
    $result = $db->query($sql);
    while ($row = mysqli_fetch_array($result, MYSQLI_BOTH)){
        
        for($i=0; $i<count($row); $i++){
            
            echo $fila[$i].' ';
        }
        echo '<br>';
    }
    echo '<br>';
    mysqli_free_result($result);
    closeDB($db);
}

function getPrecio(){
    
    $db = connectDB();
    $sql = "SELECT equipo, precio, cantidad, talla FROM Playeras WHERE precio < 1200";
    $result = $db->query($sql);
    $table = '
        <thread>
            <tr>
                <th>Equipo</th>
                <th>Precio</th>
                <th>Cantidad</th>
                <th>Talla</th>
                
            </tr>
        </thread>
        <tbody>';
    
    while ($row = mysqli_fetch_array($result, MYSQLI_BOTH)){
        
        $table .= '
        <tr>
            <td>'.$row["equipo"].'</td>
            <td>'.$row["precio"].'</td>
            <td>'.$row["cantidad"].'</td>
            <td>'.$row["talla"].'</td>    
        </tr>';
    }
    
    mysqli_free_result($result);
    closeDB($db);
    $table .= "</tbody></table>";
    return $table;
}

function getTallaPlayeras(){
    
    $db = connectDB();
    $sql = "SELECT equipo, precio, cantidad, talla FROM Playeras WHERE talla = 'mediana'";
    $result = $db->query($sql);
    $table = '
        <thread>
            <tr>
                <th>Equipo</th>
                <th>Precio</th>
                <th>Cantidad</th>
                <th>Talla</th>
                
            </tr>
        </thread>
        <tbody>';
    
    while ($row = mysqli_fetch_array($result, MYSQLI_BOTH)){
        
        $table .= '
        <tr>
            <td>'.$row["equipo"].'</td>
            <td>'.$row["precio"].'</td>
            <td>'.$row["cantidad"].'</td>
            <td>'.$row["talla"].'</td>    
        </tr>';
    }
    
    mysqli_free_result($result);
    closeDB($db);
    $table .= "</tbody></table>";
    return $table;
}


?>
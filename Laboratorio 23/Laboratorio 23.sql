CREATE TABLE ClientesBanca
(
  noCuenta VARCHAR(5) NOT NULL PRIMARY KEY,
  nombre VARCHAR(30),
  saldo NUMERIC(10,2)
)

CREATE TABLE TiposMovimiento
(
  claveM VARCHAR(2) NOT NULL PRIMARY KEY,
  descripcion VARCHAR(30)
)

CREATE TABLE Realizan
(
  noCuenta VARCHAR(5) FOREIGN KEY (noCuenta) REFERENCES ClientesBanca(noCuenta),
  claveM VARCHAR(2) FOREIGN KEY (claveM) REFERENCES TiposMovimiento(claveM),
  fecha DATETIME,
  monto numeric(10,2)
)

BEGIN TRANSACTION PRUEBA1
INSERT INTO ClientesBanca VALUES('001', 'Manuel Rios Maldonado', 9000);
INSERT INTO ClientesBanca VALUES('002', 'Pablo Perez Ortiz', 5000);
INSERT INTO ClientesBanca VALUES('003', 'Luis Flores Alvarado', 8000);
COMMIT TRANSACTION PRUEBA1

SELECT * FROM ClientesBanca

BEGIN TRANSACTION PRUEBA2
INSERT INTO ClientesBanca VALUES('004','Ricardo Rios Maldonado',19000);
INSERT INTO ClientesBanca VALUES('005','Pablo Ortiz Arana',15000);
INSERT INTO ClientesBanca VALUES('006','Luis Manuel Alvarado',18000);

ROLLBACK TRANSACTION PRUEBA2

BEGIN TRANSACTION PRUEBA3
INSERT INTO TiposMovimiento VALUES('A','Retiro Cajero Automatico');
INSERT INTO TiposMovimiento VALUES('B','Deposito Ventanilla');
COMMIT TRANSACTION PRUEBA3

BEGIN TRANSACTION PRUEBA4
INSERT INTO Realizan VALUES('001','A',GETDATE(),500);
UPDATE ClientesBanca SET SALDO = SALDO -500
WHERE NoCuenta='001'
COMMIT TRANSACTION PRUEBA4

BEGIN TRANSACTION PRUEBA5
INSERT INTO ClientesBanca VALUES('005','Rosa Ruiz Maldonado',9000);
INSERT INTO ClientesBanca VALUES('006','Luis Camino Ortiz',5000);
INSERT INTO ClientesBanca VALUES('001','Oscar Perez Alvarado',8000);

IF @@ERROR = 0
COMMIT TRANSACTION PRUEBA5
ELSE
BEGIN
PRINT 'A transaction needs to be rolled back'
ROLLBACK TRANSACTION PRUEBA5
END

SELECT * FROM ClientesBanca

CREATE PROCEDURE registrarRetiroCajero
	@noCuenta  VARCHAR(5),
	@monto     NUMERIC(10,2)
AS
	BEGIN TRANSACTION retiroCajero
	INSERT INTO Realizan VALUES (@noCuenta,'A',getdate(),@monto);
	UPDATE ClientesBanca SET saldo = saldo - @monto where noCuenta = @noCuenta
	IF @@ERROR = 0
	COMMIT TRANSACTION retiroCajero
	ELSE
	BEGIN
	PRINT 'A transaction needs to be rolled back'
    ROLLBACK TRANSACTION retiroCajero
	END
GO

EXECUTE registrarRetiroCajero '001', 500.00

SELECT * FROM ClientesBanca
SELECT * FROM Realizan

CREATE PROCEDURE registrarDepositoVentanilla
	@noCuenta  VARCHAR(5),
	@monto     NUMERIC(10,2)
AS
	BEGIN TRANSACTION depositoVentanilla
    INSERT INTO Realizan VALUES (@noCuenta, 'B', getdate(), @monto);
	UPDATE ClientesBanca SET saldo = saldo + @monto where noCuenta = @noCuenta
	IF @@ERROR = 0
    COMMIT TRANSACTION registrarDepositoVentanilla
	ELSE
	BEGIN
	PRINT 'A transaction needs to be rolled back'
	ROLLBACK TRANSACTION registrarDepositoVentanilla
	END
GO

EXECUTE registrarDepositoVentanilla '001', 500.00

SELECT * FROM ClientesBanca
SELECT * FROM Realizan

<?php 

// define variables and set to empty values
$error_nombre = $error_email = $error_telefono = $error_url = "";
$nombre = $email = $telefono = $url = $success = "";

//form is submitted with POST method
if ($_SERVER["REQUEST_METHOD"] == "POST") {
  if (empty($_POST["nombre"])) {
    $error_nombre = "El campo Nombre es obligatorio";
  } else {
    $nombre = test_input($_POST["nombre"]);
    // checa si el nombre solo contiene letras y espacio
    if (!preg_match("/^[a-zA-Z ]*$/",$nombre)) {
      $name_error = "Solo letras y espacio son caracteres validos"; 
    }
  }

  if (empty($_POST["email"])) {
    $error_email = "El campo de Correo electronico es obligatorio";
  } else {
    $email = test_input($_POST["email"]);
    // checa si el correo electronico esta bien hecho
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
      $error_email = "Invalid email format"; 
    }
  }
  
  if (empty($_POST["telefono"])) {
    $error_telefono = "El campo Telefono es obligatorio";
  } else {
    $telefono = test_input($_POST["telefono"]);
    // checa que el telefono este bien escrito
    if (!preg_match("/^(\d[\s-]?)?[\(\[\s-]{0,2}?\d{3}[\)\]\s-]{0,2}?\d{3}[\s-]?\d{4}$/i",$telefono)) {
      $error_telefono = "El numero de telefono debe contener 10 digitos"; 
    }
  }

  if (empty($_POST["url"])) {
    $error_url = "";
  } else {
    $url = test_input($_POST["url"]);
    // checa que la sintaxis del URL sea correcta
    if (!preg_match("/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i",$url)) {
      $error_url = "URL invalido"; 
    }
  }
  
  if ($error_nombre == '' and $error_email == '' and $error_telefono == '' and $error_url == '' ){
      include 'Satisfactorio.php';
  }
  
}

function test_input($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}
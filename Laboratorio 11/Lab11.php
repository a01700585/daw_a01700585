<?php include('Lab11process.php'); ?> 
<link rel = "stylesheet" href="Lab11.css" type="text/css">  
<div class="container">  
  <form id="contact" action="<?= $_SERVER["PHP_SELF"]; ?>" method="post">
    <h3>LAB 11</h3>
    <h4>Llena los campos para registrarte.</h4>
    <fieldset>
      <input placeholder="Nombre" type="text" name="nombre" value="<?= $nombre ?>" tabindex="1" autofocus>
      <span class="error"><?= $error_nombre ?></span>
    </fieldset>
    <fieldset>
      <input placeholder="Correo electronico" type="text" name="email" value="<?= $email ?>" tabindex="2">
      <span class="error"><?= $error_email ?></span>
    </fieldset>
    <fieldset>
      <input placeholder="Numero telefonico" type="text" name="telefono" value="<?= $telefono ?>" tabindex="3">
      <span class="error"><?= $error_telefono ?></span>
    </fieldset>
    <fieldset>
      <input placeholder="Sitio web comenzando con https:// " type="text" name="url" value="<?= $url ?>" tabindex="4" >
      <span class="error"><?= $error_url ?></span>
    </fieldset>
    <fieldset>
      <button name="submit" type="submit" id="contact-submit" data-submit="...Enviando">Submit</button>
    </fieldset>
    <div class="success"><?= $success ?></div>
  </form>
</div>

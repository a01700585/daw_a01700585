SET DATEFORMAT ymd

SELECT sum(Cantidad) as cantidad_total, sum((Costo+PorcentajeImpuesto)*Cantidad) as importe_total
FROM Entregan e, Materiales m
WHERE e.Clave = m.Clave AND Fecha BETWEEN '1997-01-01' AND '1997-12-31'

SELECT RazonSocial, count(Cantidad) as cantidad_total, sum((Costo+PorcentajeImpuesto)*Cantidad) as importe_total
FROM Proveedores p, Entregan e, Materiales m
WHERE e.RFC = p.RFC AND m.Clave = e.Clave
GROUP BY RazonSocial

SELECT e.Clave, Descripcion, sum(Cantidad) as Cantidad_total, min(Cantidad) as Cantidad_minima,
  max(Cantidad) as Cantidad_maxima, sum((Costo+PorcentajeImpuesto)*Cantidad) as Importe_total
FROM Materiales m, Entregan e
WHERE e.clave = m.clave
GROUP BY e.Clave, Descripcion, Cantidad
HAVING avg(Cantidad) > 400
ORDER BY Clave


SELECT RazonSocial, e.clave, Descripcion, avg(Cantidad) as promedio_cantidad
FROM Proveedores p, Materiales m, Entregan e
WHERE e.Clave = m.Clave AND p.RFC = e.RFC
GROUP BY RazonSocial, e.Clave, Descripcion, Cantidad
HAVING avg(Cantidad) > 500
ORDER BY RazonSocial

SELECT RazonSocial, e.clave, Descripcion, avg(Cantidad) as promedio_cantidad
FROM Proveedores p, Materiales m, Entregan e
WHERE e.Clave = m.Clave AND p.RFC = e.RFC
GROUP BY RazonSocial, e.Clave, Descripcion, Cantidad
HAVING avg(Cantidad) > 450 OR avg(Cantidad) < 370
ORDER BY RazonSocial

INSERT INTO Materiales VALUES (1440, 'Desarmador plano', 45.00, 7.2);
INSERT INTO Materiales VALUES (1450, 'Desarmador cruz', 45.00, 7.2);
INSERT INTO Materiales VALUES (1460, 'Martillo', 55.00, 8.8);
INSERT INTO Materiales VALUES (1470, 'Casco Construccion', 120.00, 19.2);
INSERT INTO Materiales VALUES (1480, 'Cinta de aislar', 30.00, 4.8);

SELECT clave, descripcion
FROM Materiales m
WHERE clave NOT IN
      (
        SELECT e.Clave
        FROM Materiales m, Entregan e
        WHERE m.Clave = e.Clave
      )

SELECT DISTINCT RazonSocial
FROM Proveedores p, Proyectos pr, Entregan e
WHERE pr.Numero = e.Numero AND p.RFC = e.RFC AND Denominacion = 'Queretaro Limpio'
      AND RazonSocial IN  (
                            SELECT p.RazonSocial
                            FROM Proveedores p, Proyectos pr, Entregan e
                            WHERE pr.Numero = e.Numero AND p.RFC = e.RFC AND Denominacion = 'Vamos Mexico'
                          )

SELECT descripcion
FROM Materiales m, Entregan e, Proyectos p
WHERE m.Clave = e.Clave AND p.Numero = e.Numero AND Descripcion NOT IN (
                            SELECT descripcion
                            FROM Materiales m, Entregan e, Proyectos p
                            WHERE m.Clave = e.Clave AND p.Numero = e.Numero AND Denominacion = 'CIT Yucatan'
                          )

SELECT RazonSocial, avg(Cantidad) as Promedio_cantidad
FROM Entregan e, Proveedores p
WHERE p.RFC = e.RFC
GROUP BY RazonSocial
HAVING avg(Cantidad) > (
                            SELECT avg(cantidad)
                            FROM Entregan e, Proveedores p
                            WHERE p.RFC = e.RFC AND e.RFC = 'VAGO780901'
)

SELECT e.RFC, RazonSocial
FROM Proveedores p, Proyectos pr, Entregan e
WHERE p.RFC = e.RFC AND pr.Numero = e.Numero AND Denominacion = 'Infonavit Durango'
  AND fecha BETWEEN '2000-01-01' AND '2000-12-31'
GROUP BY  RazonSocial, e.RFC
HAVING  sum(Cantidad) < (
                          SELECT sum(Cantidad)
                          FROM Proveedores p, Proyectos pr, Entregan e
                          WHERE p.RFC = e.RFC AND pr.Numero = e.Numero AND Denominacion = 'Infonavit Durango'
                                AND fecha BETWEEN '2001-01-01' AND '2001-12-31'
                        )

CREATE VIEW infoDurango2000 AS
  (
      SELECT p.RFC, p.RazonSocial, sum(Cantidad) as cantidades
      FROM Proveedores p, Proyectos pr, Entregan e
      WHERE p.RFC = e.RFC AND pr.Numero = e.Numero AND Denominacion = 'Infonavit Durango'
        AND fecha BETWEEN '2000-01-01' AND '2000-12-31'
      GROUP BY p.RFC, p.RazonSocial
  )

CREATE VIEW infoDurango2001 AS
  (
      SELECT p.RFC, p.RazonSocial, sum(Cantidad) as cantidades
      FROM Proveedores p, Proyectos pr, Entregan e
      WHERE p.RFC = e.RFC AND pr.Numero = e.Numero AND Denominacion = 'Infonavit Durango'
        AND fecha BETWEEN '2001-01-01' AND '2001-12-31'
      GROUP BY p.RFC, p.RazonSocial
  )

SELECT infoDurango2000.RFC, infoDurango2000.RazonSocial, infoDurango2000.cantidades
FROM infoDurango2000, infoDurango2001
WHERE infoDurango2000.RFC = infoDurango2001.RFC AND infoDurango2000.cantidades < infoDurango2001.cantidades
IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'Materiales')
drop table Materiales

CREATE TABLE Materiales
(
  Clave numeric(5) not null,
  Descripcion varchar(50),
  Costo numeric (8,2)
)

BULK INSERT A1700585.A1700585.[Materiales]
  FROM 'e:\wwwroot\a1700585\materiales.csv'
  WITH
  (
    CODEPAGE = 'ACP',
    FIELDTERMINATOR = ',',
    ROWTERMINATOR = '\n'
  )

INSERT INTO Materiales values(1000, 'xxx', 1000)

SELECT clave, descripcion, costo
From Materiales
--where costo=1000 and descripcion='xxx' and clave=1000

Delete from Materiales where Clave = 1000 and Costo = 1000

ALTER TABLE Materiales add constraint llaveMateriales PRIMARY KEY (Clave)

sp_helpconstraint materiales

SELECT * from Materiales


IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'Proveedores')
drop table Proveedores

CREATE TABLE Proveedores
(
  RFC char(13) not null,
  RazonSocial varchar(50)
)

BULK INSERT A1700585.A1700585.[Proveedores]
  FROM 'e:\wwwroot\A1700585\proveedores.csv'
  WITH
  (
    CODEPAGE = 'ACP',
    FIELDTERMINATOR = ',',
    ROWTERMINATOR = '\n'
  )

ALTER TABLE Proveedores add constraint llaveProveedores PRIMARY KEY (RFC)

SELECT * from Proveedores

sp_helpconstraint Proveedores


IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'Proyectos')
DROP TABLE Proyectos

CREATE TABLE Proyectos
(
  Numero numeric(5) not null,
  Denominacion varchar(50)
)

BULK INSERT A1700585.A1700585.[Proyectos]
  FROM 'e:\wwwroot\A1700585\proyectos.csv'
  WITH
  (
    CODEPAGE = 'ACP',
    FIELDTERMINATOR = ',',
    ROWTERMINATOR = '\n'
  )

ALTER TABLE Proyectos add constraint llaveProyectos PRIMARY KEY (Numero)

SELECT * from Proyectos

sp_helpconstraint Proyectos


IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'Entregan')
drop table Entregan

CREATE TABLE Entregan
(
  Clave numeric(5) not null,
  RFC char(13) not null,
  Numero numeric(5) not null,
  Fecha DateTime not null,
  Cantidad numeric (8,2)
)

SET DATEFORMAT dmy

BULK INSERT A1700585.A1700585.[Entregan]
  FROM 'e:\wwwroot\A1700585\entregan.csv'
  WITH
  (
    CODEPAGE = 'ACP',
    FIELDTERMINATOR = ',',
    ROWTERMINATOR = '\n'
  )

ALTER TABLE Entregan add constraint llaveEntregan PRIMARY KEY (Clave, RFC, Numero, Fecha)

INSERT INTO entregan values (0, 'xxx', 0, '1-jan-02', 0) ;

Delete from Entregan where Clave = 0

ALTER TABLE entregan add constraint cfentreganclave
  foreign key (clave) references materiales(clave)

ALTER TABLE entregan add constraint cfentreganRFC
  foreign key (RFC) references Proveedores(RFC)

ALTER TABLE entregan add constraint cfentreganNumero
  foreign key (Numero) references Proyectos (Numero)

INSERT INTO entregan values (1000, 'AAAA800101', 5000, GETDATE(), 0)

SELECT clave, RFC, Numero, fecha, cantidad
from Entregan
where Clave=1000 and RFC = 'AAAA800101' and Numero=5000 and Cantidad = 0

Delete from Entregan where Cantidad = 0

ALTER TABLE entregan add constraint cantidad check (cantidad > 0)


BULK INSERT a1700585.a1700585.[Materiales]
   FROM 'e:\wwwroot\a1700585\materiales.csv'
   WITH
      (
         CODEPAGE = 'ACP',
         FIELDTERMINATOR = ',',
         ROWTERMINATOR = '\n'
      )
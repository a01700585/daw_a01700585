BULK INSERT a1700585.a1700585.[Proveedores]
   FROM 'e:\wwwroot\a1700585\proveedores.csv'
   WITH
      (
         CODEPAGE = 'ACP',
         FIELDTERMINATOR = ',',
         ROWTERMINATOR = '\n'
      )
function tabla() {
    let div = document.getElementById("ej1");
    let num = prompt("Ingresa un número");
    let tbl = document.createElement("table");
    tbl.border = '1';
    let tblbody = document.createElement("tbody");
    tbl.appendChild(tblbody);
    for (let i = 1; i <= num; i++){
        let tr = document.createElement("tr");
        let td = document.createElement("td");
        td.appendChild(document.createTextNode(i)); //Crear un texto dentro de la columna
        tr.appendChild(td); //Insertar la columnda dentro de la fila
        td = document.createElement("td");
        td.appendChild(document.createTextNode(i*i));
        tr.appendChild(td);
        td = document.createElement("td");
        td.appendChild(document.createTextNode(i*i*i));
        tr.appendChild(td);
        tbl.appendChild(tr); //Incluir las filas dentro de la tabla
    }
    div.appendChild(tbl);
}

function sum(){
    let div = document.getElementById("ej2");
    let rndm1 = Math.round(Math.random()*10);
    let rndm2 = Math.round(Math.random()*10);
    let res = rndm1+rndm2;
    let inicio = new Date().getTime();
    let ans = prompt ("Cual es el resultado de la siguiente operacion "+rndm1+"+"+rndm2+"?");
    let fin = new Date().getTime();
    fin = (fin - inicio)/1000;
    if (res==ans){
        alert("Respuesta correcta, tardaste "+fin+" segundos.");
    }else{
        alert("Respuesta incorrecta, tardaste "+fin+" segundos.");
    }
}    

function contador(){
    let div = document.getElementById("ej3");
    let positivos = 0;
    let negativos = 0;
    let ceros = 0;
    let arreglo = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
    for (let i=1; i<11; i++){
        arreglo[i] = prompt ("Dame el elemento numero "+i+" del arreglo: ");
        if(arreglo[i] > 0){
            positivos++;
        } 
        if (arreglo[i] < 0){
            negativos++;
        }
        if (arreglo[i]==0){
            ceros++;
        }
    }
    alert("Hay "+positivos+" numeros positivos, hay "+negativos+" numeros negativos y "+ceros+" ceros");
}

function promedios(){
    let div = document.getElementById("ej4");
    arreglo = [[5, 7, 4, 2, 5], [6, 1, 9, 8, 3], [9, 5, 7, 0, 6]];
    let ans = " ";
    let suma = 0;
    for (let i = 0; i < arreglo.length; i++) {
        for (let j = 0; j < arreglo[i].length; j++) {
            suma += arreglo[i][j];
        }
        let promedio = suma / (arreglo[i].length);
        ans += "<br>";
        let j=i+1;
        ans += "Promedio "+ j +" = " + promedio;
    }
    div.innerHTML = ans;
 }

function inverso(){
    let div = document.getElementById("ej5");
    let n = prompt("Dame un  numero de mas de un digito");
    ans=(n+"").split("").reverse().join("");
    div.innerHTML = ans;
}
